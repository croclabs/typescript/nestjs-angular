import { Controller, Get } from '@nestjs/common';

@Controller('api')
export class ApiController {
    @Get("test")
    test(): string {
        return "test";
    }
}
