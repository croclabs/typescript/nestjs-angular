import { Component } from '@angular/core';
import { ThemeService } from './services/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  currentTheme = 'dark';

  constructor(
    private theme: ThemeService
  ) { }

  switchTheme() {
    if (this.currentTheme === 'dark') {
      this.currentTheme = 'light';
    } else {
      this.currentTheme = 'dark';
    }

    this.theme.switch(this.currentTheme);
  }
}
